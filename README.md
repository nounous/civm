# CIVM

Virtual machine bootstrapping tool using Cloud-Init.

## User guide

The user guide is WIP.

## Contributing

See [contributing guidelines](./CONTRIBUTING.md).

## Authors

CIVM is currently developped by members from [Crans](https://www.crans.org/)
and [Aurore](https://auro.re/) network organisations.

Main contributors:

-   Alexandre Iooss
-   Maxime Bombar
-   Michaël Paulon
-   Yohann D'Anello

## License

We believe in open source software.
This project is licensed under [GPL3.0+](./LICENSE.txt).
