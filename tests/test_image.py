# Copyright (C) 2020-2021 Crans
# SPDX-License-Identifier: GPL-3.0-or-later

"""Test module for civm.image
"""

import pytest

from civm.image import DebianImage, images


def test_debian_image():
    """Check that debian image return the URL on debian.org by default."""
    image = DebianImage()
    url = image.get_download_url()
    assert "debian.org" in url


@pytest.mark.parametrize("image", images)
def test_checksum_empty_image(image):
    """Check that checksum fails with empty image."""
    image = image()
    assert image.checksum(b"") == False
