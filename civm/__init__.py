# Copyright (C) 2020-2021 Crans
# SPDX-License-Identifier: GPL-3.0-or-later

"""
Virtual machine bootstrapping tool using Cloud-Init.
"""

try:
    from abby.version import version as __version__
except ImportError:
    __version__ = "dev"

# See https://www.python.org/dev/peps/pep-0008/#module-level-dunder-names
__all__ = [
    "__version__",
]
