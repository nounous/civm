# Copyright (C) 2020-2021 Crans
# SPDX-License-Identifier: GPL-3.0-or-later

"""
Main script for CIVM.
"""


def main(opt):
    # Download image using Crans mirror
    image = options.image(mirror="https://ftps.crans.org/cdimage-debian/cloud/")
    root_image = tempfile.NamedTemporaryFile()
    root_image.write(image.download())

    # Split /var
    var_image = tempfile.NamedTemporaryFile()
    civm.image.create(var_image.path)
    civm.image.format(var_image.path)
    civm.image.split_var(root_image.path, var_image.path)


if __name__ == "__main__":
    # Arguments parser
    parser = argparse.ArgumentParser(
        description="Create a new Cloud-Init virtual machine using Proxmox.",
    )
    parser.add_argument(
        "-q",
        "--quiet",
        action="store_true",
        default=False,
        help="silent mode: hide info and warnings, overrides debug",
    )
    parser.add_argument(
        "--debug",
        action="store_true",
        default=False,
        help="debug mode: show debug messages",
    )
    parser.add_argument(
        "-n",
        "--name",
        required=True,
        help="name of the new virtual machine",
    )
    parser.add_argument(
        "-i",
        "--id",
        default=None,
        help="id of the new virtual machine, default to automatic",
    )
    parser.add_argument(
        "-ss",
        "--slash-size",
        default=None,
        help="size of the new virtual machine slash partition",
    )
    parser.add_argument(
        "-vs",
        "--var-size",
        default=None,
        help="size of the new virtual machine var partition",
    )
    parser.add_argument(
        "-m",
        "--memory",
        default=None,
        help="size of the new virtual machine virtual memory",
    )
    parser.add_argument(
        "-c",
        "--cpu-count",
        default=None,
        help="amount of cpu cores of the new virtual machine",
    )
    parser.add_argument(
        "--image",
        default=civm.image.DebianImage,
        type=civm.image.get,
        help="select image to use",
    )
    options = parser.parse_args()
    main(options)
