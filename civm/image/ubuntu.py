# Copyright (C) 2020-2021 Crans
# SPDX-License-Identifier: GPL-3.0-or-later

"""
Ubuntu images support.
"""

import hashlib
import logging
import re

import requests

from civm.image.base import Image

# Local logger
log = logging.getLogger(__name__)


class UbuntuImage(Image):
    """Ubuntu image."""

    name = "ubuntu"
    user_account = "ubuntu"

    def __init__(
        self,
        version="focal",
        arch="amd64",
        mirror="https://cloud-images.ubuntu.com/releases/",
        image_format="qcow2",
    ):
        """Ubuntu image.

        :param version: Ubuntu version codename, defaults to "focal"
        :type version: str, optional
        :param arch: architecture, defaults to "amd64"
        :type arch: str, optional
        :param mirror: Ubuntu cloud images mirror to use, defaults to
            "https://cloud-images.ubuntu.com/releases/"
        :type mirror: str, optional
        :param image_format: format of the image, defaults to "qcow2"
        :type image_format: str, optional
        :raises IndexError: if version is unknown
        """
        # Normalize version
        if version in ["focal", "20.04"]:
            self.version = "focal"
            self.version_id = "20.04"
        else:
            raise IndexError("Unknown version")

        # .img is qcow2
        if image_format == "qcow2":
            image_format = "img"

        self.mirror = mirror
        self.arch = arch
        self.imagename = (
            f"ubuntu-{self.version_id}-server-cloudimg-{arch}.{image_format}"
        )

    def checksum(self, image) -> bool:
        """Checksum given image

        :param image: image content
        :type image: bytes
        :raises IndexError: if checksum not found
        :return: true if checksum is correct
        :rtype: bool
        """
        # Retrive checksum
        manifest_url = f"{self.mirror}{self.version}/release/SHA256SUMS"
        r = requests.get(manifest_url, allow_redirects=True)
        m = re.search(fr"(\w+)\s+\*?{re.escape(self.imagename)}\s+", r.text)
        if not m:
            raise IndexError("Checksum not found")
        checksum = m.group(1)

        # Check
        log.debug(f"Computing checksum")
        h = hashlib.sha256(image)
        return h.hexdigest() == checksum

    def get_download_url(self) -> str:
        """Get download URL of this image.

        :return: download URL
        :rtype: str
        """
        return f"{self.mirror}{self.version}/release/{self.imagename}"
