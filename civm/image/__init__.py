# Copyright (C) 2020-2021 Crans
# SPDX-License-Identifier: GPL-3.0-or-later

"""
Implement supported Cloud-Init images.
"""

import subprocess

from civm.image.base import Image
from civm.image.debian import DebianImage
from civm.image.fedora import FedoraImage
from civm.image.ubuntu import UbuntuImage

__all__ = ["images", "DebianImage", "FedoraImage", "UbuntuImage", "get_image"]

images = [
    DebianImage,
    FedoraImage,
    UbuntuImage,
]


def get(name: str) -> Image:
    """Get a block cipher by name

    :param name: name of the block cipher to get
    :type name: str
    :raises ValueError: cipher not found
    :return: block cipher object
    :rtype: blockcipher.BlockCipher
    """
    for image in images:
        if image.name == name:
            return image
    raise ValueError(f"{name} is not implemented")


def create(path: str, image_format="qcow2", size="5G"):
    """Create new disk image with qemu-img.

    :param path: path of the new image
    :type path: str
    :param image_format: image format, defaults to "qcow2"
    :type image_format: str, optional
    :param size: image size, defaults to "5G"
    :type size: str, optional
    """
    log.info(f"Creating {size} {image_format} file at {path}")
    subprocess.run(["qemu-img", "create", "-f", image_format, path, size])


def format(path: str, partition_format="ext4"):
    """Create a new MBR partition table with one partition inside the image.

    :param path: path of the image to process
    :type path: str
    :param partition_format: new partition format, defaults to "ext4"
    :type partition_format: str, optional
    """
    # Launch VM
    from guestfs import GuestFS

    g = GuestFS(python_return_dict=True)
    g.add_drive_opts(path)
    g.launch()

    # Format
    log.info(f"Creating MBR partition table in {path}")
    g.part_disk("/dev/sda", "mbr")
    log.info(f"Formating first partition in {path} as {partition_format}")
    g.mkfs(partition_format, "/dev/sda1")


def split_var(root_image_path: str, var_image_path: str):
    """Split ``/var`` from root image into another image.

    :param root_image_path: path of the root image, usually a Cloud-Init qcow2
        image.
    :type root_image_path: str
    :param var_image_path: path of the image which will contain ``/var``.
    :type var_image_path: str
    """
    # Launch VM with two images
    from guestfs import GuestFS

    g = GuestFS(python_return_dict=True)
    g.add_drive_opts(root_image_path)
    g.add_drive_opts(var_image_path)
    g.launch()

    # Move /var
    logger.info("Moving content from root image /var to var image")
    g.mount("/dev/sda1", "/")
    g.mount("/dev/sdb1", "/mnt")
    g.sh("mv /var/* /mnt")

    # Add var image to fstab
    logger.info("Adding /var partition uuid to root image fstab")
    uuid = g.get_e2uuid("/dev/sdb1")
    new = f"UUID={uuid}\t/var\text4\tdefaults\t\t0\t2\n"
    g.write_append("/etc/fstab", new)
