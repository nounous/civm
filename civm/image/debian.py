# Copyright (C) 2020-2021 Crans
# SPDX-License-Identifier: GPL-3.0-or-later

"""
Debian images support.
"""

import hashlib
import logging
import re

import requests

from civm.image.base import Image

# Local logger
log = logging.getLogger(__name__)


class DebianImage(Image):
    """Debian image."""

    name = "debian"
    user_account = "debian"

    def __init__(
        self,
        version="current",
        arch="amd64",
        mirror="http://cdimage.debian.org/cdimage/",
        image_format="qcow2",
    ):
        """Debian image.

        :param version: Debian version, can be numerical, defaults to "current"
        :type version: str, optional
        :param arch: architecture, defaults to "amd64"
        :type arch: str, optional
        :param mirror: Debian CD images mirror to use, defaults to
            "http://cdimage.debian.org/cdimage/"
        :type mirror: str, optional
        :param image_format: format of the image, defaults to "qcow2"
        :type image_format: str, optional
        :raises IndexError: if version is unknown
        """
        # Normalize version
        if version in ["stable", "current", "10"]:
            self.version = "current"
            self.version_id = "10"
        else:
            raise IndexError("Unknown version")

        self.mirror = mirror
        self.arch = arch
        self.imagename = f"debian-{self.version_id}-openstack-{arch}.{image_format}"

    def checksum(self, image) -> bool:
        """Checksum given image

        :param image: image content
        :type image: bytes
        :raises IndexError: if checksum not found
        :return: true if checksum is correct
        :rtype: bool
        """
        # Retrive checksum
        manifest_url = f"{self.mirror}openstack/{self.version}/SHA512SUMS"
        r = requests.get(manifest_url, allow_redirects=True)
        m = re.search(fr"(\w+)\s+{re.escape(self.imagename)}\s+", r.text)
        if not m:
            raise IndexError("Checksum not found")
        checksum = m.group(1)

        # Check
        log.debug(f"Computing checksum")
        h = hashlib.sha512(image)
        return h.hexdigest() == checksum

    def get_download_url(self) -> str:
        """Get download URL of this image.

        :return: download URL
        :rtype: str
        """
        return f"{self.mirror}openstack/{self.version}/{self.imagename}"
