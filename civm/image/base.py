# Copyright (C) 2020-2021 Crans
# SPDX-License-Identifier: GPL-3.0-or-later

"""
Common interface for all images.
"""

import logging

import requests

# Local logger
log = logging.getLogger(__name__)


class Image:
    """Operating system image."""

    # Image identifier
    name = None

    # Default user inside the image
    user_account = None

    def checksum(self, image) -> bool:
        """Checksum given image

        :param image: image content
        :type image: bytes
        :raises IndexError: if checksum not found
        :return: true if checksum is correct
        :rtype: bool
        """
        raise NotImplementedError()

    def get_download_url(self) -> str:
        """Get download URL of this image.

        :return: download URL
        :rtype: str
        """
        raise NotImplementedError()

    def download(self, checksum=True) -> bytes:
        """Download image

        :param checksum: checksum the image, defaults to True
        :type checksum: bool, optional
        :raises ValueError: if checksum mismatch
        :return: image
        :rtype: bytes
        """
        url = self.get_download_url()
        log.info(f"Getting image from {url}")
        r = requests.get(url, allow_redirects=True)
        if not self.checksum(r.content):
            raise ValueError("Checksum mismatched")
        return r.content
