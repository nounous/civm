# Copyright (C) 2020-2021 Crans
# SPDX-License-Identifier: GPL-3.0-or-later

"""
Fedora images support.
"""

import hashlib
import logging
import re

import requests

from civm.image.base import Image

# Local logger
log = logging.getLogger(__name__)


class FedoraImage(Image):
    """Fedora image.

    See <https://alt.fedoraproject.org/cloud/> for reference.
    """

    name = "fedora"
    user_account = "fedora"

    def __init__(
        self,
        version="33",
        arch="x86_64",
        mirror="https://download.fedoraproject.org/pub/fedora/linux/",
        image_format="qcow2",
    ):
        """Fedora image.

        :param version: Fedora version, should be numerical, defaults to "33"
        :type version: str, optional
        :param arch: architecture, defaults to "x86_64"
        :type arch: str, optional
        :param mirror: Fedora images mirror to use, defaults to
            "https://download.fedoraproject.org/pub/fedora/linux/"
        :type mirror: str, optional
        :param image_format: format of the image, defaults to "qcow2"
        :type image_format: str, optional
        :raises IndexError: if version is unknown
        """
        # Normalize version
        if not version.isnumeric():
            raise IndexError("Unknown version")

        self.version = version
        self.arch = arch
        self.mirror = mirror
        self.imagename = f"Fedora-Cloud-Base-{self.version}-1.2.{arch}.{image_format}"

    def checksum(self, image) -> bool:
        """Checksum given image

        :param image: image content
        :type image: bytes
        :raises IndexError: if checksum not found
        :return: true if checksum is correct
        :rtype: bool
        """
        # Retrive checksum
        manifest_url = f"{self.mirror}releases/{self.version}/Cloud/{self.arch}/images/Fedora-Cloud-{self.version}-1.2-{self.arch}-CHECKSUM"
        r = requests.get(manifest_url, allow_redirects=True)
        m = re.search(fr"(\w+)\s+\({re.escape(self.imagename)}\)\s=\s(\w+)", r.text)
        if not m:
            raise IndexError("Checksum not found")
        checksum_type, checksum = m.group(1), m.group(2)
        if checksum_type != "SHA256":
            raise NotImplementedError("Only SHA256 is supported")

        # Check
        log.debug(f"Computing checksum")
        h = hashlib.sha256(image)
        return h.hexdigest() == checksum

    def get_download_url(self) -> str:
        """Get download URL of this image.

        :return: download URL
        :rtype: str
        """
        return f"{self.mirror}releases/{self.version}/Cloud/{self.arch}/images/{self.imagename}"
